EffectNodes for Cocos2dx
==========
by yang chao (wantnon), 2014-6-26  

EffectNodes for Cocos2dx is a collection of 2d special effects implemented with cocos2dx. i will add more when i got good effects.  
  
EffectNodes is based on cocos2d-x 2.2.0   
   
currently, iOS, Mac and Win32 project are available.  
  
how to run the demo: http://git.oschina.net/wantnon2/EffectNodes-for-Cocos2dx/wikis/how-to-run-the-demo  
如何运行demo: http://git.oschina.net/wantnon2/EffectNodes-for-Cocos2dx/wikis/如何运行demo  
  
here is executable for win32 and mac: http://git.oschina.net/wantnon2/EffectNodes-for-Cocos2dx/attach_files  
  
screenshots:  
  
![screenshot](http://git.oschina.net/wantnon2/EffectNodes-for-Cocos2dx/raw/master/resource/screenshots/frontPage.png)  
  
![screenshot](http://git.oschina.net/wantnon2/EffectNodes-for-Cocos2dx/raw/master/resource/screenshots/lightningBolt.png)   
  
![screenshot](http://git.oschina.net/wantnon2/EffectNodes-for-Cocos2dx/raw/master/resource/screenshots/break.png)      
    
![screenshot](http://git.oschina.net/wantnon2/EffectNodes-for-Cocos2dx/raw/master/resource/screenshots/normalMapped.png)    
  

qq group: 338565878   
  
blog: http://user.qzone.qq.com/350479720/blog/1403785410  
  